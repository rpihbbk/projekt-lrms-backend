module.exports = (items) => (body, cb) => {
  items.remove({ _id: body.id }, err => cb(err || 'success'))
}

const hash = require('../hash.js')
const { root_pw_hash } = require('../config.js')

module.exports = users => (body, cb) => {
  if(body.username === 'root' && hash(body.password) === root_pw_hash) {
    cb({ _id: 1, username: 'root', fullname: 'root' })
  } else {
    users.findOne({ username: body.username, password: hash(body.password) }, (err, doc) => {
      if(err) {
        console.log(err)
        cb(null)
      } else {
        cb(doc)
      }
    })
  }
}

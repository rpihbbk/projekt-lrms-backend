module.exports = (events) => (body, cb) => {
  if(body.date && body.date.start && body.date.end) {
    if(body.item !== undefined) {
      events.find({ $and:
        [{ date: { $gte: body.date.start }}, { date: { $lte: body.date.end }}, { item: body.item }]
      }, cb)
    } else {
      events.find({ $and:
        [{ date: { $gte: body.date.start }}, { date: { $lte: body.date.end }}]
      }, cb)
    }
  }
  if(body.id)
    events.findOne({_id: body.id}, cb)
  else if(body.title)
    events.findOne({title: body.title}, cb)
}

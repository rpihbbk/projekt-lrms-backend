const create_update = body => {
  let l_obj = {}
  ;['teacher', 'item', 'start', 'end', 'date', 'room', 'title', 'recurring', 'recurring_time'].map(x => {
    if(x in body)
      l_obj[x] = body[x]
  })
  console.log(l_obj)
  return l_obj
}

module.exports = (events) => (body, cb) => {
  events.update({ _id: body.id }, { '$set': create_update(body) }, {}, cb)
}

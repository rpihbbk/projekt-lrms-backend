const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const Datastore = require('nedb')
const app = express()
const router = express.Router()

const _timetable = require('./package.json').timetable

const setTime = (date, hours=0, minutes=0, seconds=0, milliseconds=0, offset=true) =>
  new Date(new Date(date).setHours(+hours, +minutes - (offset ? date.getTimezoneOffset() : 0), +seconds, +milliseconds))

const mapObj = (_obj, fn) => {
  let obj = Object.assign({}, _obj)
  Object.keys(obj)
    .map(key => fn(obj[key], key))
    .forEach((new_val, i) => obj[Object.keys(obj)[i]] = new_val)
  return obj
}

const timetable = mapObj(_timetable, obj => ({
    start: setTime(new Date(), ...obj.start.split('-').map(x => +x), 0, false),
    end: setTime(new Date(), ...obj.end.split('-').map(x => +x), 0, false)
  }))


const startTime = +new Date()

let { root_pw_hash, secret, port } = require('./config.js')
port = port || process.env.PORT || 8080

const middleware = require('./middleware.js')(jwt, secret)

const Users = new Datastore({ filename: 'db/users.db', autoload: true })
const Events = new Datastore({ filename: 'db/events.db', autoload: true })
const Items = new Datastore({ filename: 'db/items.db', autoload: true })


const user = {
  get: require('./users/get_user.js')(Users),
  add: require('./users/add_user.js')(Users),
  mod: require('./users/mod_user.js')(Users),
  del: require('./users/del_user.js')(Users),
  vfy: require('./users/vfy_user.js')(Users),
  namelookup: require('./users/namelookup.js')(Users),
  idlookup: require('./users/idlookup.js')(Users)
}

const events = {
  get: require('./events/get_event.js')(Events),
  add: require('./events/add_event.js')(Events),
  mod: require('./events/mod_event.js')(Events),
  del: require('./events/del_event.js')(Events),
}

const items = {
  get: require('./items/get_item.js')(Items),
  add: require('./items/add_item.js')(Items),
  mod: require('./items/mod_item.js')(Items),
  del: require('./items/del_item.js')(Items),
}

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(middleware.cors)

app.options("/*", function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.sendStatus(200);
});

// its just so fucking stupid that i need to do this, why does this FUCKING OPTIONS METHOD THINGY EVEN EXIST!?!?!? it serves no fucking purpose other than annoying the hell out of everybody

router.use('/user/:sub_route', middleware.users)
router.use('/namelookup/:id', middleware.events)
router.use('/idlookup/:name', middleware.events)
router.use('/item/:sub_route', middleware.items)
router.use('/event/:sub_route', middleware.events)

router.use('/testauth', middleware.auth_user)
router.use('/me', middleware.auth_user)

app.use(router)


const sign_token = (jwt_user, secret, time='1h', cb) => {
  user.get({username: jwt_user.username}, (doc) => {
    console.log(doc)
    cb(jwt.sign({
      id: jwt_user.id,
      username: jwt_user.username,
      userId: doc._id
    }, secret, { expiresIn: time }))
  })
}


router.post('/auth', (req, res) => {
  console.log(req.body)
  user.vfy(req.body, (l_user) => {
    if(l_user) {
      sign_token(l_user, secret, '1h', msg => res.send(msg))
    } else {
      res.status(401).send('Incorrect username and/or password')
    }
  })
})

router.get('/testauth', (req, res) => res.send(res.locals.jwt))

router.get('/me', (req, res) => res.send(res.locals.jwt))


router.post('/user/add', (req, res) => user.add(req.body, msg => res.send(msg)))

router.post('/user/del', (req, res) => user.del(req.body, msg => res.send(msg)))

router.post('/user/mod', (req, res) => user.mod(req.body, msg => res.send(msg)))

router.post('/user/get', (req, res) => user.get(req.body, msg => res.send(msg)))

router.get('/namelookup/:id', (req, res) => user.namelookup(req.params.id, msg => res.send(msg)))

router.get('/idlookup/:name', (req, res) => (console.log(req.params.name), user.idlookup(req.params.name, msg => res.send(msg))))

router.get('/status', (req, res) => {
  res.json({
    running: true,
    system_start_date: startTime,
    system_time: +new Date(),
    uptime: +new Date() - startTime
  })
})


app.get('/item_in_use/:id', (req, res) => {
  res.header('Content-Type', 'application/json')
  items.get({ id: req.params.id }, msg => {
    if(typeof(msg) === 'string') res.send(msg)
    const date = new Date()
    getCurrentEvent(date, +req.params.id, event => {
      msg.event = event
      msg.reserved = !!event
      res.send(msg)
    })
  })
})

const getCurrentEvent = (date, item, cb) => {

  const index = Object.values(timetable).reduce((sum, {start, end}, i) =>
    start <= date && date <= end ? i : sum
  , undefined)

  const hour = Object.keys(timetable)[index]

  // console.log('index', index)
  // console.log('Object.keys(timetable)[index]', Object.keys(timetable)[index])
  // console.log('Object.values(timetable)[index]', Object.values(timetable)[index])

  events.get({
    date: {
      start: setTime(date, 0, 0, 0, 0).toISOString(),
      end:   setTime(date, 23, 59, 59, 999).toISOString()
    },
    item: item
  }, (err, doc) => {
    if(err) cb('error')
    cb(doc
      .map(event => event.start <= hour && hour <= event.end ? event : null)
      .filter(event => event ? event : null)[0])
  })
}


router.post('/event/add', (req, res) => {
  const transform = (body, personId) => ({
    teacher: personId,
    item: body.item,
    start: body.start,
    end: body.end ? body.end : body.start,
    room: body.room,
    title: body.title || body.name || null,
    date: body.date,
    recurring: body.recurring || false,
    recurring_time: body.recurring ? body.recurring_time : null
  })

  if(['item', 'start', 'end', 'room', 'person', 'recurring'].map(x => x in req.body).filter(x => !x).length === 0) {
    if(res.locals.jwt.username === 'root') {
      console.log(req.body, req.body.person || '1')
      events.add(transform(req.body, req.body.person || '1'), (err, doc) => res.send(err || (doc ? doc._id : null)))
    } else {
      user.get({username: res.locals.jwt.username}, doc => {
        if(doc !== null) {
          console.log(req.body, doc._id)
          events.add(transform(req.body, doc._id), (err, doc) => res.send(err || (doc ? doc._id : null)))
        } else {
          res.send('error')
        }
      })
    }
  } else {
    console.log('not enough parameters')
    console.log(req.body)
    res.send('error')
  }
})

router.post('/event/del', (req, res) => {
  events.get(req.body, (err, doc) => {
    if(err)
      res.send('error')
    else if(res.locals.jwt.username === 'root' || res.locals.jwt.userId === doc.teacher)
      events.del(req.body, err => res.send(err || 'success'))
  })
})

router.post('/event/get', (req, res) => {
  events.get(req.body, (err, doc) => {
    if(err) res.send('error')
    else res.send(doc)
  })
})

router.post('/event/mod', (req, res) => {
  events.get(req.body, (err, doc) => {
    console.log(req.body, err, doc)
    if(err)
      res.send('error1')
    else if(res.locals.jwt.username === 'root' || res.locals.jwt.userId === doc.teacher) {
      const obj = Object.assign({teacher: req.body.person || req.body.teacher}, req.body)
      events.mod(obj, (err, numReplaced) => res.send(err || 'success' + numReplaced))
    } else {
      res.send('error2')
    }
  })
})


router.post('/item/add', (req, res) => items.add(req.body, msg => res.send(msg)))

router.post('/item/del', (req, res) => items.del(req.body, msg => res.send(msg)))

router.post('/item/get', (req, res) => items.get(req.body, msg => res.send(msg)))

router.post('/item/mod', (req, res) => items.mod(req.body, msg => res.send(msg)))

/*

post:/auth {
  body { username: [username],
    password: [password]
  }
} -> JWT

get:/testauth {
  header {
    authorization: [JWT]
  }
} -> user obj

post:/user/get {
  header {
    authorization: [JWT]
  }
  body {
    username: [username] || id: [id]
  }
} -> user obj

post:/user/add {
  header {
    authorization: [JWT]
  }
  body {
    username: [username],
    fullname: [fullname],
    password: [password]
  }
} -> user added -> user obj

post:/user/del {
  header {
    authorization: [JWT]
  }
  body {
    id: [id]
  }
} -> user deleted -> success

post:/user/mod {
  header {
    authorization: [JWT]
  }
  body {
    id: [id],
    new_user: {
      username: [username],
      fullname: [fullname],
      password: [password]
    }
  }
} -> user modified -> success

*/

app.listen(port, () => {
  console.log(`server started on ${port}`)
})

const create_event = body => ({
  teacher: body.teacher,
  item: +body.item,
  start: +body.start,
  end: +body.end,
  date: body.date,
  room: +body.room,
  title: body.title,
  recurring: !!body.recurring,
  recurring_time: body.recurring_time !== null ? +body.recurring_time : null,
})

module.exports = (events) => (body, cb) => {
  events.insert(create_event(body), cb)
}

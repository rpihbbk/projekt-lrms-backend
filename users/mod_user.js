const hash = require('../hash.js')

const update_user = body => {
  let l_obj = {}
  ;['username', 'fullname', 'password', 'created_at'].map(x => {
    if(x in body)
      l_obj[x] = x === 'password' ? hash(body[x]) : body[x]
  })
  return l_obj
}

module.exports = users => (body, cb) => {
  users.update({ _id: body.id }, { '$set': update_user(body) }, err => cb(err ? 'error' : 'success'))
}

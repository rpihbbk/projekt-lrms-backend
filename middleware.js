const middleware_fn = (jwt, secret) => {

  const middleware = {
    auth_root: (req, res, next) => middleware.users(req, res, next),
    auth_user: (req, res, next) => middleware.events(req, res, next),
    users: (req, res, next) => {
      if(!req.headers['authorization']) {
        res.status(401).send('no JWT')
      } else {
        jwt.verify(req.headers['authorization'], secret, (err, decoded) => {
          if(err || decoded.username !== 'root') {
            res.status(401).send('not authorized')
          } else {
            res.locals.jwt = decoded
            next()
          }
        })
      }
    },
    events: (req, res, next) => {
      if(!req.headers['authorization']) {
        res.status(401).send('no JWT')
      } else {
        jwt.verify(req.headers['authorization'], secret, (err, decoded) => {
          if(err) {
            res.status(401).send('not authorized')
          } else {
            res.locals.jwt = decoded
            next()
          }
        })
      }
    },
    items: (req, res, next) => {
      if(!req.headers['authorization']) {
        res.status(401).send('no JWT')
      } else {
        jwt.verify(req.headers['authorization'], secret, (err, decoded) => {
          if(!err) {
            if(req.params.sub_route === 'get' || decoded.username === 'root') {
              res.locals.jwt = decoded
              next()
            } else {
              res.status(401).send('not authorized')
            }
          } else {
            res.status(401).send('not authorized')
          }
        })
      }
    },
    cors: (req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      next()
    },
  }
  return middleware
}

module.exports = middleware_fn

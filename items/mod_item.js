const update_item = body => {
  let l_obj = {}
  ;[].map(x => {  // add all possible fields for item
    if(x in body)
      l_obj[x] = body[x]
  })
  return l_obj
}

module.exports = (items) => (body, cb) => {
  items.update({ _id: body.id }, { '$set': update_item(body) }, err => cb(err ? 'error' : 'success'))
}

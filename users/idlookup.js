module.exports = user => (name, cb) => {
  if(name === 'root') {
    cb({ id: '1' })
  } else {
    user.findOne({ username: name }, (err, doc) => (console.log(err, doc, name), cb(err || !doc ? 'error' : ({ id: doc._id }))))
  }
}

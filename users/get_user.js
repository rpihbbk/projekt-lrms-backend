module.exports = user => (body, cb) => {
  let obj = body.username
    ? { username: body.username}
    : { _id: body.id}
  if(body.username === 'root' || body.id === "1" || body.id === 1) { // IDs are usually not numbers, but since the ID of root is set to "1" the Integer representation is also accepted (also that is the form that the ID is returned in in the case of root)
    cb({ _id: '1', username: 'root', fullname: 'root' })
  } else {
    user.findOne(obj, (err, doc) => cb(err ? 'error' : doc))
  }
}

const hash = require('../hash.js')

const create_user = body => ({
  username: body.username,
  fullname: body.fullname,
  password: hash(body.password),
  created_at: Date.now()
})

module.exports = (users) => (body, cb) => {
  users.insert(create_user(body), (err, doc) => cb(err || doc._id))
}

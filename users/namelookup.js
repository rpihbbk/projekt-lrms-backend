module.exports = user => (id, cb) => {
  if(id === "1" || id === 1) {
    cb({ username: 'root' })
  } else {
    user.findOne({ _id: id }, (err, doc) => cb(err || !doc ? 'error' : ({ username: doc.username })))
  }
}

module.exports = users => (body, cb) => {
  let obj = body.id
            ? { _id: body.id }
            : body.username
              ? { username: body.username }
              : null
  if(!obj) {
    cb('username or id needed')
    return null
  }
  users.remove(obj, err => cb(err ? 'error' : 'success'))
}
